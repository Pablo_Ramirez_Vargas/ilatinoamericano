"""ilatinoamericano URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from mymessages.views import send_message,thanks
from informacion import urls
from usuarios import urls
from calificaciones import urls
from django.contrib.auth import views as auth_views
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('send_message/',send_message, name="send_message"),
    url(r'profile/',include('usuarios.urls'),name='usuarios'),
    url(r'cal/',include('calificaciones.urls'),name='cali'),
    url(r'^',include('informacion.urls'),name='informacion'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
