from django import forms
from .models import my_user
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm

class User_registerform(ModelForm):
    class Meta:
        model = my_user
        fields=['nombre','email','direcion','phone','password']
class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
