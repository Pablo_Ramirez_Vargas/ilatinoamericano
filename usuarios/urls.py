from django.conf.urls import url
from .views import register,login_user
from django.contrib.auth import views as auth_views

urlpatterns = [
url(r'^login/$',login_user, name='login'),
url(r'^logout/$', auth_views.LogoutView.as_view(template_name='index_view.html'), name='logout'),
url('register/',register,name='register'),
]
