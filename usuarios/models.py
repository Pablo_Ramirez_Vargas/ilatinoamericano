from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,BaseUserManager)
class UserManager(BaseUserManager):
    def create_user(self,email,nombre,phone,direcion,password,is_staff=False,is_superuser=False,is_active=True):
        if not email:
            raise ValueError('email es requerido')
        if not nombre:
            raise ValueError("nombre requerido")
        if not phone:
            raise ValueError("un telefono es requerido")
        if not direcion:
            raise ValueError("una direcion es requerida")
        if not password:
            raise ValueError('un password es requerido')
        user_obj=self.model(
        email=self.normalize_email(email)
        )
        user_obj.set_password(password)
        user_obj.nombre=nombre
        user_obj.phone=phone
        user_obj.direcion=direcion
        user_obj.staff=is_staff
        user_obj.superuser=is_superuser
        user_obj.active=is_active
        user_obj.save()
        return user_obj
    def create_staff(self,email,nombre,phone,direcion,password):
        user=self.create_user(email,
        nombre=nombre,
        phone=phone,
        direcion=direcion,
        staff=True,superuser=False)
        return user
    def create_superuser(self,email,nombre,phone,direcion,password,is_superuser=True,is_staff=True):
        user=self.create_user(email,
        nombre=nombre,phone=phone,password=password,
        is_staff=is_staff,
        direcion=direcion,is_superuser=True)
        return user
#the costum user
class my_user(AbstractBaseUser):
    email= models.EmailField(max_length=255,unique=True,null=False)
    staff=models.BooleanField(default=False)
    nombre=models.CharField(max_length=100)
    phone= models.CharField(max_length=100)
    direcion=models.CharField(max_length=100)
    superuser=models.BooleanField(default=False)
    USERNAME_FIELD='email'
    REQUIRED_FIELDS=['nombre','phone','direcion']
    objects=UserManager()
    def __str__(self):
        return self.email
    def get_full_name(self):
        return self.email
    def get_short_name(self):
        return self.email
    def has_perm(self,perm,obj=None):
        return True
    def has_module_perms(self,app_label):
        return True
    @property
    def is_staff(self):
        return self.staff
    @property
    def is_superuser(self):
        return self.superuser
