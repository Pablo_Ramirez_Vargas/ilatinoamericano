from django.shortcuts import render
from django.contrib.auth import login, authenticate
from .forms import  User_registerform,LoginForm

# Create your views here.
def login_user(request):
    if request.method == "POST":
        form = LoginForm(request.POST or None)
        if form.is_valid():
          username = form.cleaned_data['email']
          password = form.cleaned_data['password']
          print(username, password)
          user = authenticate(request,username=username, password=password)
          print(user)
          if user and user.is_active:
             login(request, user)
          else:
              print('f')
    else:
         form = LoginForm()
    return render(request, "login.html",{'form':form})
        # Redirect to a success page.

def register(request,*args, **kwargs):
    if request.user.is_superuser:
        registered=False
        if request.method=='POST':
            user_form=User_registerform(data=request.POST)
            if user_form.is_valid():
                user=user_form.save(commit=False)
                user.staff=False
                user.admin=False
                user.set_password(user.password)
                user.is_active = True
                user.save()
                print(user,user.password)
                login(request, user)
                user.save()
                registere=True
        else:
            user_form =  User_registerform()
        return render(request,'register.html',{'user_form':user_form})
    else:
        return login_user(request)
