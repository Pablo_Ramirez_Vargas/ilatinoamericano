# Generated by Django 2.2.3 on 2019-08-26 20:27

from django.db import migrations
import usuarios.models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0012_auto_20190826_1521'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='my_user',
            managers=[
                ('objects', usuarios.models.UserManager()),
            ],
        ),
    ]
