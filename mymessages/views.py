from django.shortcuts import render
from .forms import mymessageform
from django.contrib.auth import login as auth_login
from django.core.mail import send_mail
# Create your views here.
def thanks(request,*args,**kwargs):
        return render(request,'thanks.html',{})
def send_message(request,*args,**kwargs):
    if request.method == 'POST':
        form = mymessageform(request.POST)
        if form.is_valid():
            form.save()
            return thanks(request)
        else:
            form = mymessageform()
    return render(request, 'mymessageform.html', {"form":form})

    # if a GET (or any other method) we'll create a blank form
