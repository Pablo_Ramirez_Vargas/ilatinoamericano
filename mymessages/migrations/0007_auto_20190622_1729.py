# Generated by Django 2.2.2 on 2019-06-22 22:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mymessages', '0006_auto_20190619_1543'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mymessage',
            old_name='body',
            new_name='mensage',
        ),
    ]
