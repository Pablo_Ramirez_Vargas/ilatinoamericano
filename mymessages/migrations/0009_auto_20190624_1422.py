# Generated by Django 2.2.2 on 2019-06-24 19:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mymessages', '0008_auto_20190624_1421'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mymessage',
            name='curso',
            field=models.CharField(choices=[('bachillerato', 'bachillerato'), ('Licenciatura', 'Licenciatura'), ('ingles', 'ingles'), ('informatica', 'informatica')], max_length=100),
        ),
    ]
