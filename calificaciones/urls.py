from django.conf.urls import url
from .views import calificaciones11,calificaciones12,calificaciones13,calificaciones14,calificaciones15,calificaciones16,all_cal,imprimir
urlpatterns = [
url('calificaciones1/',calificaciones11, name="calificaciones1"),
url('calificaciones2/',calificaciones12, name="calificaciones2"),
url('calificaciones3/',calificaciones13, name="calificaciones3"),
url('calificaciones4/',calificaciones14, name="calificaciones4"),
url('calificaciones5/',calificaciones15, name="calificaciones5"),
url('calificaciones6/',calificaciones16, name="calificaciones6"),
url('all_cal/',all_cal, name="all_cal"),
url('imprimir',imprimir,name='imprimir'),
]
