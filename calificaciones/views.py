# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import calificaciones1,calificaciones2,calificaciones3,calificaciones4,calificaciones5,calificaciones6
from django.shortcuts import get_list_or_404, get_object_or_404
import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table,TableStyle,Image,SimpleDocTemplate
from reportlab.lib import colors
from reportlab.lib.units import inch

# Create your views here.
def calificaciones11(request,extrac=False):
    current_user = request.user
    request.session['view']=1
    calificaciones=get_object_or_404(calificaciones1,owner_id=current_user.id)
    calificaciones_fields=calificaciones.__dict__
    calificaciones_fields.pop('_state')
    calificaciones_fields.pop('id')
    calificaciones_fields.pop('owner_id')

    for key,value in calificaciones_fields.items():
        calificaciones_fields[key.replace('_',' ')]=calificaciones_fields.pop(key)
    if extrac==True:
        return {"calificaciones":calificaciones_fields}
    else:
        return render(request, 'cali.html', {"calificaciones":calificaciones_fields})
def calificaciones12(request,extrac=False):
        current_user = request.user
        request.session['view']=1
        calificaciones=get_object_or_404(calificaciones2,owner_id=current_user.id)
        calificaciones_fields=calificaciones.__dict__
        calificaciones_fields.pop('_state')
        calificaciones_fields.pop('id')
        calificaciones_fields.pop('owner_id')

        for key,value in calificaciones_fields.items():
            calificaciones_fields[key.replace('_',' ')]=calificaciones_fields.pop(key)
        if extrac==True:
            return {"calificaciones":calificaciones_fields}
        else:
            return render(request, 'cali.html', {"calificaciones":calificaciones_fields})
def calificaciones13(request,extrac=False):
        current_user = request.user
        request.session['view']=1
        calificaciones=get_object_or_404(calificaciones3,owner_id=current_user.id)
        calificaciones_fields=calificaciones.__dict__
        calificaciones_fields.pop('_state')
        calificaciones_fields.pop('id')
        calificaciones_fields.pop('owner_id')

        for key,value in calificaciones_fields.items():
            calificaciones_fields[key.replace('_',' ')]=calificaciones_fields.pop(key)
        if extrac==True:
            return {"calificaciones":calificaciones_fields}
        else:
            return render(request, 'cali.html', {"calificaciones":calificaciones_fields})
def calificaciones14(request,extrac=False):
        current_user = request.user
        request.session['view']=1
        calificaciones=get_object_or_404(calificaciones4,owner_id=current_user.id)
        calificaciones_fields=calificaciones.__dict__
        calificaciones_fields.pop('_state')
        calificaciones_fields.pop('id')
        calificaciones_fields.pop('owner_id')

        for key,value in calificaciones_fields.items():
            calificaciones_fields[key.replace('_',' ')]=calificaciones_fields.pop(key)
        if extrac==True:
            return {"calificaciones":calificaciones_fields}
        else:
            return render(request, 'cali.html', {"calificaciones":calificaciones_fields})
def calificaciones15(request,extrac=False):
    current_user = request.user
    request.session['view']=1
    calificaciones=get_object_or_404(calificaciones5,owner_id=current_user.id)
    calificaciones_fields=calificaciones.__dict__
    calificaciones_fields.pop('_state')
    calificaciones_fields.pop('id')
    calificaciones_fields.pop('owner_id')

    for key,value in calificaciones_fields.items():
        calificaciones_fields[key.replace('_',' ')]=calificaciones_fields.pop(key)
    if extrac==True:
        return {"calificaciones":calificaciones_fields}
    else:
        return render(request, 'cali.html', {"calificaciones":calificaciones_fields})
def calificaciones16(request,extrac=False):
    current_user = request.user
    request.session['view']=6
    calificaciones=get_object_or_404(calificaciones6,owner_id=current_user.id)
    calificaciones_fields=calificaciones.__dict__
    calificaciones_fields.pop('_state')
    calificaciones_fields.pop('id')
    calificaciones_fields.pop('owner_id')

    for key,value in calificaciones_fields.items():
        calificaciones_fields[key.replace('_',' ')]=calificaciones_fields.pop(key)
    if extrac==True:
        return {"calificaciones":calificaciones_fields}
    else:
        return render(request, 'cali.html', {"calificaciones":calificaciones_fields})
def all_cal(request,extrac=False):
    current_user = request.user
    cali={}
    request.session['view']=7
    calificaciones=get_object_or_404(calificaciones1,owner_id=current_user.id)
    cali.update(calificaciones.__dict__)
    calificaciones=get_object_or_404(calificaciones2,owner_id=current_user.id)
    cali.update(calificaciones.__dict__)
    calificaciones=get_object_or_404(calificaciones3,owner_id=current_user.id)
    cali.update(calificaciones.__dict__)
    calificaciones=get_object_or_404(calificaciones4,owner_id=current_user.id)
    cali.update(calificaciones.__dict__)
    calificaciones=get_object_or_404(calificaciones5,owner_id=current_user.id)
    cali.update(calificaciones.__dict__)
    calificaciones=get_object_or_404(calificaciones6,owner_id=current_user.id)
    cali.pop('_state')
    cali.pop('id')
    cali.pop('owner_id')

    for key,value in cali.items():
        cali[key.replace('_',' ')]=cali.pop(key)
    if extrac==True:
        return {"calificaciones":cali}
    else:
        return render(request, 'cali.html', {"calificaciones":cali})
def imprimir(request,**kwargs):
    logo = "static/imgs/logo.png"
    im = Image(logo, 2 * inch, 2 * inch)
    buffer = io.BytesIO()
    data=[]
    data.append(["NO OFICIAL"])
    elements=[]
    if request.session['view']==1:
        b=(calificaciones11(request,extrac=True))
    if request.session['view']==2:
        b=(calificaciones12(request,extrac=True))
    if request.session['view']==3:
        b=(calificaciones13(request,extrac=True))
    if request.session['view']==4:
        b=(calificaciones14(request,extrac=True))
    if request.session['view']==5:
        b=(calificaciones15(request,extrac=True))
    if request.session['view']==6:
        b=(calificaciones16(request,extrac=True))
    if request.session['view']==7:
        b=(all_cal(request,extrac=True))
    c = SimpleDocTemplate(buffer,pagesize=letter)
    for key,it in (b['calificaciones']).items():
        data.append([key,it])
    d=(b['calificaciones'])
    data.append(["este documento se extiende con fines meramente informativos,"])
    data.append(['para control interno y para control del alumno NO tiene ningún tipo de validez.'])
    t=Table(data)
    elements.append(im)
    elements.append(t)
    c.build(elements)
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename="calificacion.pdf")
