from django.conf.urls import url
from .views import index_view,bachillerato,footer,licenciaturas,costos,especialidad,unico,dosys,unoys
urlpatterns = [
#url('thanks/',thanks, name="thanks"),
url('bachillerato/',bachillerato,name="bachillerato"),
url('footer/',footer,name="footer"),
url('licenciaturas/',licenciaturas,name='licenciaturas'),
url('costos/',costos,name='costos'),
url('especialidad/',especialidad,name='especialidad'),
url('unico/',unico,name="unico"),
url('dosy6/',dosys,name="dosys"),
url('unoy8/',unoys,name="unoys"),
url(r"^$",index_view,name='index_view'),
#url('register/',register,name='register'),
]
