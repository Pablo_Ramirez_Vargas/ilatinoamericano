# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from mymessages import views as msg_Views
# Create your views here.
def index_view(request,*args,**kwargs):
    form = msg_Views.mymessageform()
    return render(request,"index_view.html",{ "form":form,"user":request.user})
def bachillerato(request,*args,**kwargs):
    return render(request,"bachillerato.html",{})

def footer(request,*args,**kwargs):
    return render(request,"footer.html",{})

def licenciaturas(request,*args,**kwargs):
    return render(request,"licenciaturas.html",{})

def costos(request,*args,**kwargs):
    return render(request,"costos.html",{})

def especialidad(request,*args,**kwargs):
    return render(request,"especialidades.html",{})

def unico(request,*args,**kwargs):
    return render(request,"unico.html",{})

def dosys(request,*args,**kwargs):
    return render(request,"dosys.html",{})

def unoys(request,*args,**kwargs):
    return render(request,"unoys.html",{})
